#include <stdio.h> 
#include <stdbool.h>
#include <stm8s.h>
#include "main.h"
#include "milis.h"
#include "uart1.h"
#include "daughterboard.h"
#include "adc_helper.h"




#define PIN_IR GPIOG, GPIO_PIN_2
#define PIN_IRSNS GPIOG, GPIO_PIN_3

#define BAUDVAL 8334 // baudrate 9600 b/s

#define IR_ON GPIO_WriteHigh(PIN_IR); // zapne IR LED
#define IR_OFF GPIO_WriteLow(PIN_IR); // vypne --//--

#define GET_IR ADC_get(CHANNEL_VTEMP) // Zjisti hodnotu na IR snímači



void init(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);  // taktovani MCU na 16MHz

    GPIO_Init(PIN_IR, GPIO_MODE_OUT_PP_LOW_SLOW); // inicializace
    GPIO_Init(PIN_IRSNS, GPIO_MODE_IN_PU_NO_IT);

    init_milis();
    init_uart1();

    // na pinech vypneme vstupní buffer
    ADC2_SchmittTriggerConfig(ADC2_SCHMITTTRIG_CHANNEL14, DISABLE); 
    ADC2_SchmittTriggerConfig(ADC2_SCHMITTTRIG_CHANNEL15, DISABLE); 
    // nastavíme CLOCK pro ADC2 (16MHz / 4 = 4 MHz)
    ADC2_PrescalerConfig(ADC2_PRESSEL_FCPU_D4);
    // zvolím zarovnání výsledku -- většinou doprava
    ADC2_AlignConfig(ADC2_ALIGN_RIGHT);
    // nastavíme multiplexer na některý kanál
    ADC2_Select_Channel(ADC2_CHANNEL_14);
    // rozběhnu ADC2
    ADC2_Cmd(ENABLE);
    // počkamá až se ADC2 rozběhne (~7 us)
    ADC2_Startup_Wait();


}


int main(void)
{
    uint32_t time = 0;
    bool pruchod = false;
    uint16_t pocetpruchodu = 0;

    init();

    while(1)
    {
        if(milis()-time > 20)
        {
            time = milis();
            
            // IR LED se spustí
            IR_ON;  

            // pokud je hodnota dost vysoká, tak mezi IR LED a snímačem nic není
            if(GET_IR > 15) 
            {
                pruchod = false;
            }

            // pokud je naopak hodnota moc nízká, tak mezi IR LED a snímačem je překážka
            else    
            {
                if(!pruchod)
                {
                    pocetpruchodu++;
                    printf("\n\nPocet pruchodu: %d", pocetpruchodu);
                }
                pruchod = true;
            }

             // IR LED se vypne
            IR_OFF;
        }
    }
}

/*-------------------------------  Assert -----------------------------------*/
#include "__assert__.h"