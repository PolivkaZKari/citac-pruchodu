### Závěrečný projekt mit - Počítadlo průchodů ###

    ## Zadani ##
    Počítá kolik lidí prošlo dovnitř a ven pomocí dvojice optických IR bran

    ## Potreby ##
    IR LED a IR detektory

    ## Schema, flow chart a zdrojovy kod ##
    Vse je v git repozitari, zde jsou odkazy na jednotlive veci:

     Schema: https://gitlab.com/PolivkaZKari/citac-pruchodu/-/blob/main/schemacitacpruchodu.pdf?ref_type=heads

     Flow chart: https://gitlab.com/PolivkaZKari/citac-pruchodu/-/blob/main/flowchart.pdf?ref_type=heads

     Zdrojovy kod: https://gitlab.com/PolivkaZKari/citac-pruchodu/-/blob/main/citacpruchodu/src/main.c?ref_type=heads

    ## Popis programu ##
    1) provede se inicializace
    2) spusti se nekonecny while cyklus v hlavnim programu
    3) spusti se IR LED
    4) zjisti se hodnota na IR snimaci
        4.1) pokud je moc vysoka (na IR snimac dopada IR svetlo), nic se nestane
        4.2) pokud je dost nizka (mezi IR snimacem a IR LED je prekazka), program pricte k poctu pruchodu 1 a vypise pocet pruchodu
    5) vypne se IR LED
    6) cyklus se opakuje
